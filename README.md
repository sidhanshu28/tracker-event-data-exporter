# msfExportNew

External app for dhis2, designed completely in JavaScript, HTML and CSS.

- created to get tracker data of patients enrolled in the system.

- export the data in three different formats.

  - Excel (xls / xlsx)
  - JSON file
  - HTML format 
